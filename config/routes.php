<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes)
{

    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => false
    ]));

    $routes->setExtensions(['json']);

    $routes->applyMiddleware('csrf');

    $routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    $routes->connect('/users', ['controller' => 'Users', 'action' => 'view']);

    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    $routes->fallbacks(DashedRoute::class);
});

Router::scope('/articles', function (RouteBuilder $routes)
{
    $routes->connect('/tagged/*', ['controller' => 'Articles', 'action' => 'tags']);
});
