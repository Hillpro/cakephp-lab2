<?php
namespace App\Controller;

use App\Controller\AppController;

class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
    }

    /*public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));

    }*/

    public function view()
    {
        $id = $this->Auth->user('id');

        $user = $this->Users->get($id, [
            'contain' => ['Articles'],
        ]);

        $this->set('user', $user);

        $this->set('_serialize', ['user']);
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            $image = $this->request->data['image'];

            if($image['name'] != '')
            {
                $filepath = $image['tmp_name'];
                $filesize = $image['size'];

                $handle = fopen($filepath, 'rb');

                $content = fread($handle, $filesize);
                fclose($handle);
                $image_data = base64_encode($content);

                $user->image = $image_data;
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            $image = $this->request->data['image'];

            if($image['name'] != '')
            {
                $filepath = $image['tmp_name'];
                $filesize = $image['size'];

                $handle = fopen($filepath, 'rb');

                $content = fread($handle, $filesize);
                fclose($handle);
                $image_data = base64_encode($content);

                $user->image = $image_data;
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($this->Auth->user('id'));

        if ($this->Users->delete($user))
        {
            $this->Flash->success(__('The user has been deleted.'));
            return $this->redirect(['action' => 'logout']);
        }
        else
        {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'logout']);
    }

    public function login()
    {
        if($id = $this->Auth->user('id'))
            {
                return $this->redirect(['action' => 'view']);
            }

        if($this->request->is('post'))
        {
            $user = $this->Auth->identify();

            if($user)
            {
                $this->Auth->setUser($user);
                //return $this->redirect($this->Auth->redirectUrl());
                return $this->redirect(['action' => 'view']);
            }

            $this->Flash->error('Your username or password is incorrect');
        }
    }

    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

    public function isAuthorized($user)
  {
    $action = $this->request->getParam('action');

    if(in_array($action, ['logout', 'add', 'view']))
    {
        return true;
    }

    $id = $this->request->getParam('pass.0');

    return $id == $user['id'];
  }
}
