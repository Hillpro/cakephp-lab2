<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Comment extends Entity
{
    protected $_accessible = [
        'comment' => true,
        'created' => true,
        'article_id' => true,
        'user_id' => true,
        'article' => true,
        'user' => true,
    ];
}
